const express = require("express");
const cookieParser = require('cookie-parser')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const morgan = require('morgan');
const morganBody = require('morgan-body')

const meals = require('./modules/meals')
const users = require('./modules/users')
const auth = require('./modules/auth')

const app = express();
const port = 3000
const dbUrl = 'mongodb://user:testarosa19@ds251507.mlab.com:51507/meals-planner';

mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
    console.log('mongo db connection', err)
})

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
morganBody(app);
app.use(session({
    secret: 'testarosa',
    require: true,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}))
app.use(auth.initialize)
app.use(auth.session)
app.use(auth.setUser)

app.use('/', auth.router)
app.use(async (req, res, next) => {
    if (req.isAuthenticated())
        return next();
    res.status(400).json({
        'message': 'access denied'
    });
})
app.use('/meal', meals)
app.use('/user', users)

app.listen(process.env.PORT || 3000, () =>
    console.log(`Example app listening on port ${port}!`)
)