var express = require('express');
var router = express.Router();
const User = require('../models/user')
const Meal = require('../models/meal')
const { bcrypt, saltRounds } = require('./util')
const { handleError } = require('../modules/util')

router.get('/', async (req, res) => {
    if (req.user.role == 0) {
        res.send(401)
        return
    }

    try {
        var users = await User
            .find({})
            .select('-password -createdAt -updatedAt -__v')
            .exec()
            .then((users) => {
                return users
            })
        res.status(200).json(users)
    } catch (err) {
        return handleError(err, res)
    }
})

router.get('/:id', async (req, res) => {
    if (req.user.role == 0 && req.params.id != req.user._id) {
        res.send(401)
        return
    }

    try {
        var user = await User
            .findOne({ _id: req.params.id })
            .select('-password -createdAt -updatedAt -__v')
            .exec()
            .then((user) => {
                return user
            })
        res.status(200).json(user)
    } catch (err) {
        return handleError(err, res)
    }
})

router.delete('/:id', (req, res) => {
    if (req.user.role == 0) {
        res.send(401)
        return
    }
    User.deleteOne({ _id: req.params.id }, (err) => {
        if (err) return handleError(err, res)
        Meal.deleteMany({ userId: req.params.id }, (err) => {
        })
        res.sendStatus(200)
    })
})

router.put('/:id', async (req, res) => {
    if (req.user.role == 0 && req.params.id != req.user._id) {
        res.send(401)
        return
    }

    if (req.body.password) {
        req.body.password = await bcrypt.hash(req.body.password, saltRounds)
    } else {
        delete req.body.password
    }

    try {
        await User.findOneAndUpdate({ _id: req.params.id }, req.body).exec()
        var user = await User
            .findOne({ _id: req.params.id })
            .select('-password -createdAt -updatedAt -__v')
            .exec()
        res.status(200).json(user)
    } catch (err) {
        handleError(err, res)
    }
})

module.exports = router