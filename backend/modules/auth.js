var express = require('express');
var router = express.Router();
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const User = require('../models/user')
const { handleError } = require('../modules/util')
const { check } = require('express-validator');

passport.use(
    new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
        function  (req, email, password, done) {
            User.findOne({ email: email }, async function (err, user) {
                if (err) { return done(err); }
                if (!user) { return done(null, false, { message: 'Invalid username or password' }); }
                var passwordValid = await user.comparePassword(password)
                if (!passwordValid) { return done(null, false, { message: 'Invalid username or password' }); }
                req.user = user
                return done(null, user);
            });
        }
    ));


passport.serializeUser((user, done) => {
    return done(null, user.id)
})
passport.deserializeUser(async (id, done) => {
    try {
        const user = await User.findById(id).exec()
        return done(null, user)
    } catch (err) {
        return done(err)
    }
})

router.post('/user', [
    check('name').isLength({ min: 1 }).withMessage('name can\'t be empty'),
    check('email').isEmail().withMessage('invalid email address'),
    check('password').isLength({ min: 5 }).withMessage('password must be 5 chars long')
], async (req, res, next) => {
    var user = new User(req.body)
    user.role = 0
    try {
        var user = await user.save()
        return next()
    } catch (err) {
        return next(err)
    }
}, passport.authenticate('local')
    , function (req, res) {
        User
            .findOne({ _id: req.user.id })
            .select('-password -createdAt -updatedAt -__v')
            .exec((err, user) => {
                if (err) return handleError(err, res)
                res.status(200).json(user)
            })
    })

router.post('/login',
    passport.authenticate('local'),
    function (req, res) {
        User
            .findOne({ _id: req.user.id })
            .select('-password -createdAt -updatedAt -__v')
            .exec((err, user) => {
                if (err) return handleError(err, res)
                res.status(200).json(user)
            })
    }
)

router.get('/loginFailed', (req, res) => {
    res.status(401)
})

router.get('/profile', (req, res) => {
    User
        .findOne({ _id: req.user.id })
        .select('-password -createdAt -updatedAt -__v')
        .exec((err, user) => {
            if (err) return handleError(err, res)
            res.send(JSON.stringify(user))
        })
})

router.get('/logout', (req, res) => {
    req.logout();
    res.status(200).json({
        'message': 'successfully logout'
    });
});

module.exports = {
    router: router,
    initialize: passport.initialize(),
    session: passport.session(),
    setUser: (req, res, next) => {
        res.locals.user = req.user
        return next()
    }
}