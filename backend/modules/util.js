const bcrypt = require('bcrypt')
const SALT_ROUNDS = 12

const handleError = function handleError(err, res) {
    res.status(400).send(JSON.stringify(err))
}

const isLoggedIn = function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.status(400).json({
        'message': 'access denied'
    });
}
module.exports = {
    handleError: handleError,
    isLoggedIn: isLoggedIn,
    bcrypt: bcrypt,
    saltRounds: SALT_ROUNDS
}