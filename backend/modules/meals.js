const { check, validationResult } = require('express-validator');
const dateTime = require('date-and-time');
const Meal = require('../models/meal')
const express = require('express');
const router = express.Router()
const { handleError } = require('../modules/util')

function extractDocs(input) {
    var result = input.docs
    if (result.length > 0) {
        result[result.length - 1]["nextPage"] = input.nextPage
    }
    return result
}

//When logged in, a user can see a list of his meals, 
//also he should be able to add, edit and delete meals. 
//(user enters calories manually, no auto calculations!)
router.get('/', async (req, res) => {
    var userId = req.query.userId ? req.query.userId : req.user.id
    var pivotMeal = null
    var error = null
    var from = null
    if (req.query.id) {
        pivotMeal = await Meal
            .findOne({ userId: userId, _id: req.query.id })
            .select('-createdAt -updatedAt -__v')
            .exec()
            .then((meal) => {
                console.log(meal);
                return meal;
            }).catch((err) => {
                error = err
                return null;
            })
    } else {
        pivotMeal = await Meal
            .findOne({ userId: userId })
            .select('-createdAt -updatedAt -__v')
            .sort({ "datetime": -1 }).limit(1)
            .exec()
            .then((meal) => {
                console.log(meal);
                return meal;
            }).catch((err) => {
                error = err
                return null;
            })
    }
    if (!pivotMeal) {
        res.status(200).send([])
        return
    }

    if (error) return handleError(error, res)
    var pivotDate = dateTime.parse(pivotMeal.date.toLocaleDateString(), 'M/D/YYYY')
    from = req.query.id ? pivotDate : dateTime.addDays(pivotDate, 1)
    var to = req.query.id ? dateTime.addDays(from, -6) : dateTime.addDays(from, -5)
   
    console.log(`id: ${req.query.id} gte: ${to} lt: ${from}`)
    var meals = await Meal.find({
        date: {
            $gte: to, $lt: from
        },
        userId: userId
    })
        .select('-createdAt -updatedAt -__v')
        .exec()
        .then((meals) => {
            return meals
        }).catch((err) => {
            error = err
            console.error(`error: ${err}`)
            return null
        })
    if (error) return handleError(error, res)
    res.status(200).json(meals)
})

router.get('/filter', (req, res) => {
    var page = req.query.page ? req.query.page : 1
    const options = {
        page: page,
        limit: 10
    };

    var fromDate = req.query['fromDate'] //2019-10-07
    var toDate = req.query['toDate']
    var fromTime = req.query['fromTime'] //12
    var toTime = req.query['toTime']

    var from = dateTime.parse(fromDate, 'DD-MM-YYYY');
    var to = dateTime.addDays(dateTime.parse(toDate, 'DD-MM-YYYY'), 1);
    console.log(`from: ${from} to: ${to} ft: ${fromTime} tt: ${toTime}`)

    Meal.paginate({
        userId: req.user.id,
        date: { $gte: from, $lte: to },
        hour: { $gte: fromTime, $lte: toTime }
    }, options, (err, meals) => {
        if (err) return handleError(err, res)
        var docs = extractDocs(meals)
        res.status(200).json(docs)
    })
})

router.post('/', [
    // kcal must be an integer
    check('kcal').isInt().withMessage('must be an int'),
    // text must be at least 5 chars long
    check('text').isLength({ min: 1 }).withMessage('must be at least 5 chars long')

], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(422).json({ errors: errors.array() });

    var meal = new Meal(req.body)
    if (req.body.date) {
        meal.date = req.body.date
    } else {
        meal.date = new Date().toISOString()
    }

    meal.hour = parseInt(dateTime.format(meal.date, 'HH'));
    console.log('hour: ' + meal.hour)
    meal.userId = req.user.id

    meal.save((err, result) => {
        if (err) return handleError(err, res)
        res.status(200).json(result)
    })
})

router.get('/:id', (req, res) => {
    Meal.find({ _id: req.params.id }, (err, meals) => {
        if (err) return handleError(err, res)
        if (meals.length > 0) {
            res.status(200).json(meals[0])
        } else {
            res.sendStatus(404)
        }
    })
})

router.put('/:id', async (req, res) => {
    if (req.user.role == 2) {
        Meal.findOneAndUpdate({ _id: req.params.id }, req.body, (err) => {
            if (err) return handleError(err, res)
            res.sendStatus(200)
        })
    } else {
        var meal = await Meal.findOne({ _id: req.params.id })
        if (meal.userId == req.user.id) {
            Meal.findOneAndUpdate({ _id: req.params.id }, req.body, (err) => {
                if (err) return handleError(err, res)
                res.sendStatus(200)
            })
        } else {
            res.sendStatus(401)
        }
    }
})

router.delete('/:id', async (req, res) => {
    if (req.user.role == 2) {
        Meal.deleteOne({ _id: req.params.id }, (err) => {
            if (err) return handleError(err, res)
            res.sendStatus(200)
        })
    } else {
        var meal = await Meal.findOne({ _id: req.params.id })
        if (meal.userId == req.user.id) {
            Meal.deleteOne({ _id: req.params.id }, (err) => {
                if (err) return handleError(err, res)
                res.sendStatus(200)
            })
        } else {
            res.sendStatus(401)
        }

    }

})

module.exports = router