const mongoose = require('mongoose')
const emailValidator = require('email-validator')
const {bcrypt, saltRounds} = require('../modules/util')
var UserSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        minlength: 1
    },
    goal: {
        type: Number,
        default: 2000
    },
    email: {
        type: String,
        required: true,
        trim: true,
        index: { unique: true },
        validate: {
            validator: emailValidator.validate,
            message: props => `${props.value} is not valid email address`
        },
        minlength: 1
    },
    password: {
        type: String,
        required: true,
        trim: true,
        minlength: 5
    },
    role: {
        type: Number,
        required: true,
        default: 0
    }
}, {
    timestamps: true
})

UserSchema.pre('save', async function preSave(next) {
    const user = this;
    if (!user.isModified('password')) return next()
    try {
        const hash = await bcrypt.hash(user.password, saltRounds)
        user.password = hash
        return next()
    } catch (err) {
        return next(err)
    }
})

UserSchema.methods.comparePassword = async function comparePassword(candidate) {
    return bcrypt.compare(candidate, this.password)
}

var User = mongoose.model('User', UserSchema)

module.exports = User