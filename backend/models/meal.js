const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2');

var MealSchema = mongoose.Schema({
    text: String,
    kcal: Number,
    date: Date,
    hour: Number,
    userId: String,
    nextPage: String
})

MealSchema.plugin(mongoosePaginate)
var Meal = mongoose.model('Meal', MealSchema)

module.exports = Meal