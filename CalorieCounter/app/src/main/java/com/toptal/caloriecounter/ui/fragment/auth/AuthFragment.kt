package com.toptal.caloriecounter.ui.fragment.auth

import android.os.Bundle
import android.view.View
import androidx.fragment.app.commit
import androidx.lifecycle.observe
import com.google.android.material.snackbar.Snackbar
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.entity.ResourceSuccess
import com.toptal.caloriecounter.ui.fragment.BaseFragment
import com.toptal.caloriecounter.viewmodel.AuthViewModel
import com.toptal.caloriecounter.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class AuthFragment : BaseFragment() {
    private val viewModel by lazy { provideViewModel<AuthViewModel>() }
    private val mainViewModel by lazy { provideViewModel<MainViewModel>(activity!!) }

    override fun provideLayoutId(): Int = R.layout.fragment_auth

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null) {
            val loginFragment = LoginFragment()
            childFragmentManager.commit {
                replace(R.id.container, loginFragment)
            }
        }
    }

    override fun initViewModel(savedInstanceState: Bundle?) {
        viewModel.screen.observe(viewLifecycleOwner) {
            when (it) {
                AuthViewModel.Screen.LoginScreen -> {
                    childFragmentManager.commit {
                        replace(R.id.container, LoginFragment())
                    }
                }
                AuthViewModel.Screen.SignUpScreen -> {
                    childFragmentManager.commit {
                        replace(R.id.container, SignUpFragment())
                    }
                }
            }
        }

        mainViewModel.authResult.addSource(viewModel.authResult) {
            if (it is ResourceSuccess) {
                mainViewModel.authResult.value = it.data
            } else {
                Snackbar.make(container, "Authentication error !", Snackbar.LENGTH_SHORT).show()
            }
        }
    }
}