package com.toptal.caloriecounter.repository

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.toptal.caloriecounter.api.CreateMealRequest
import com.toptal.caloriecounter.api.EditMealRequest
import com.toptal.caloriecounter.api.MealService
import com.toptal.caloriecounter.db.MealDao
import com.toptal.caloriecounter.entity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.threeten.bp.Instant
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MealRepository @Inject constructor(
    private val mealDao: MealDao,
    private val mealService: MealService
) {
    suspend fun loadAll(
        userId: String,
        pageSize: Int,
        callback: PagedList.BoundaryCallback<Meal>
    ) = withContext<LiveData<PagedList<Meal>>>(Dispatchers.IO) {
        val dataSourceFactory = mealDao.loadAllByUserId(userId)

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize)
            .build()

        val builder = LivePagedListBuilder<Int, Meal>(
            dataSourceFactory,
            pagedListConfig
        ).setBoundaryCallback(callback)

        builder.build()
    }

    suspend fun save(meals: List<Meal>) = withContext(Dispatchers.IO) {
        mealDao.insert(meals)
    }

    suspend fun clearAll() = withContext(Dispatchers.IO) {
        mealDao.clearAll()
    }

    suspend fun fetchMeals(userId: String? = null, nextPage: String? = null) =
        withContext<Resource<List<Meal>>>(Dispatchers.IO) {
            try {
                val meals = mealService.getAll(userId, nextPage)
                ResourceSuccess(meals)
            } catch (e: Exception) {
                ResourceError(e)
            }
        }

    suspend fun create(text: String, kcal: Int) = withContext<Resource<Meal>>(Dispatchers.IO) {
        try {
            val currentDateTime = OffsetDateTime.now().format(
                DateTimeFormatter.ISO_INSTANT)
            val meal = mealService.create(
                CreateMealRequest(
                    text, kcal, currentDateTime
                )
            )
            mealDao.insert(meal)
            ResourceSuccess(meal)
        } catch (e: Exception) {
            ResourceError(e)
        }
    }

    suspend fun update(meal: Meal) = withContext(Dispatchers.IO) {
        try {
            mealService.update(meal._id, EditMealRequest(meal.text, meal.kcal))
            mealDao.update(meal)
            StatusSuccess
        } catch (e: Exception) {
            StatusError
        }
    }

    suspend fun delete(meal: Meal) = withContext(Dispatchers.IO) {
        try {
            mealService.delete(meal._id)
            mealDao.delete(meal)
            StatusSuccess
        } catch (e: Exception) {
            StatusError
        }
    }

    suspend fun filter(fromDate: Long, toDate: Long, fromTime: Int, toTime: Int) =
        withContext<Resource<List<Meal>>>(Dispatchers.IO) {
            try {
                val offsetFromDate =
                    OffsetDateTime.ofInstant(Instant.ofEpochMilli(fromDate), ZoneId.systemDefault())
                        .format(
                            DateTimeFormatter.ofPattern(DATE_FORMAT)
                        )
                val offsetToDate =
                    OffsetDateTime.ofInstant(Instant.ofEpochMilli(toDate), ZoneId.systemDefault())
                        .format(
                            DateTimeFormatter.ofPattern(DATE_FORMAT)
                        )

                val meals = mealService.filter(offsetFromDate, offsetToDate, fromTime, toTime)

                ResourceSuccess(meals)
            } catch (e: Exception) {
                ResourceError(e)
            }
        }

    companion object {
        const val DATE_FORMAT = "dd-MM-uuuu"
    }
}