package com.toptal.caloriecounter.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.toptal.caloriecounter.entity.User

@Dao
interface UserDao : BaseDao<User> {
    @Query("SELECT * FROM user")
    fun load(): User

    @Query("SELECT * FROM user")
    fun loadAsSource(): LiveData<User>
}