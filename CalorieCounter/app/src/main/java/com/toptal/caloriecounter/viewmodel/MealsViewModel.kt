package com.toptal.caloriecounter.viewmodel

import androidx.lifecycle.*
import androidx.paging.PagedList
import com.hadilq.liveevent.LiveEvent
import com.toptal.caloriecounter.entity.*
import com.toptal.caloriecounter.repository.AuthRepository
import com.toptal.caloriecounter.repository.MealRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MealsViewModel @Inject constructor(
    private val mealRepository: MealRepository,
    private val authRepository: AuthRepository
) : ViewModel() {
    val user = MediatorLiveData<User>()
    private val mealsRequest = MutableLiveData<String>()
    val meals: LiveData<PagedList<Meal>> = mealsRequest.switchMap { userId ->
        liveData {
            val source =
                mealRepository.loadAll(userId, 100, object : PagedList.BoundaryCallback<Meal>() {
                    override fun onZeroItemsLoaded() {
                        refreshStatus.postValue(true)
                        fetchMeals()
                    }

                    override fun onItemAtEndLoaded(itemAtEnd: Meal) {
                        if (nextPage != itemAtEnd._id) {
                            refreshStatus.postValue(true)
                            fetchMeals(itemAtEnd._id)
                        }
                    }
                })

            emitSource(source)
        }
    }
    val mealDeleteResult = LiveEvent<Resource<Meal>>()
    val mealsFetchStatus = LiveEvent<ResourceStatus>()
    val refreshStatus = LiveEvent<Boolean>()

    private var nextPage: String? = ""
    private var fetchUserId: String? = null

    fun init(user: User?) {
        fetchUserId = user?._id
        if (user != null) {
            this.user.value = user
            mealsRequest.value = user._id
        } else {
            this.user.addSource(authRepository.loadLoggedInAsSource()) {
                if (it.prettyRole() == Role.Regular) {
                    mealsRequest.value = it._id
                }
                this.user.value = it
            }
        }
    }

    fun onRefresh() {
        fetchMeals(clearDb = true)
    }

    private fun fetchMeals(nextPage: String? = null, clearDb: Boolean = false) =
        viewModelScope.launch {
            this@MealsViewModel.nextPage = nextPage
            val resource = mealRepository.fetchMeals(fetchUserId, nextPage)
            if (resource is ResourceSuccess) {
                if (clearDb) {
                    mealRepository.clearAll()
                }
                if (resource.data.isNotEmpty()) {
                    mealRepository.save(resource.data)
                }
            } else {
                mealsFetchStatus.value = StatusError
            }
            refreshStatus.value = false
        }

    fun onMealDeleteClicked(meal: Meal) = viewModelScope.launch {
        val status = mealRepository.delete(meal)
        if (status is StatusSuccess) {
            mealDeleteResult.value = ResourceSuccess(meal)
        } else {
            mealDeleteResult.value = ResourceError(data = meal)
        }
    }
}