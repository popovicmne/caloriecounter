package com.toptal.caloriecounter.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.di.Injectable
import com.toptal.caloriecounter.entity.Role
import com.toptal.caloriecounter.entity.prettyRole
import com.toptal.caloriecounter.ui.fragment.*
import com.toptal.caloriecounter.ui.fragment.auth.AuthFragment
import com.toptal.caloriecounter.viewmodel.MainViewModel
import com.toptal.caloriecounter.viewmodel.Screen
import javax.inject.Inject


class MainActivity : AppCompatActivity(), Injectable {
    @Inject
    lateinit var injectableViewModelFactory: ViewModelProvider.Factory

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(
            this,
            injectableViewModelFactory
        ).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initUI()
        initViewModel()

        viewModel.init(savedInstanceState)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    private fun initUI() {
        supportFragmentManager.commit {
            replace(
                R.id.container,
                SplashFragment()
            )
        }
    }

    private fun initViewModel() {
        viewModel.screen.observe(this) {
            when (it) {
                Screen.SplashScreen -> {
                    supportFragmentManager.commit {
                        replace(R.id.container, SplashFragment())
                    }
                }
                is Screen.AuthScreen -> {
                    if (supportFragmentManager.backStackEntryCount > 0) {
                        supportFragmentManager.popBackStack()
                    }
                    supportFragmentManager.commit {
                        replace(R.id.container, AuthFragment())
                    }
                }
                is Screen.MealsScreen -> {
                    supportFragmentManager.commit {
                        addToBackStack(null)
                        replace(R.id.container, MealsFragment.newInstance(it.user))
                    }
                }
                is Screen.EditMealScreen -> {
                    supportFragmentManager.commit {
                        addToBackStack(null)
                        replace(R.id.container, EditMealFragment.newInstance(it.meal))
                    }
                }
                Screen.ProfileScreen -> {
                    supportFragmentManager.commit {
                        addToBackStack(null)
                        replace(R.id.container, ProfileFragment())
                    }
                }
                is Screen.FilterScreen -> {
                    supportFragmentManager.commit {
                        addToBackStack(null)
                        replace(R.id.container, FilterFragment.newInstance(it.filterRequest))
                    }
                }
            }
        }

        viewModel.authResult.observe(this) {
            if (it != null) {
                if (it.prettyRole() == Role.Regular) {
                    supportFragmentManager.commit {
                        replace(R.id.container, MealsFragment.newInstance())
                    }
                } else {
                    supportFragmentManager.commit {
                        replace(R.id.container, UsersFragment.newInstance(it))
                    }
                }
            }
        }

        viewModel.user.observe(this) {
            if (it != null) {
                if (it.prettyRole() == Role.Regular) {
                    transitionFromSplashToMeals()
                } else {
                    transitionFromSplashToUsers(it)
                }
            } else {
                transitionFromSplashToAuth()
            }
        }
    }
}