package com.toptal.caloriecounter.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.observe
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.Moshi
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.db.MoshiProvider
import com.toptal.caloriecounter.entity.*
import com.toptal.caloriecounter.hideKeyboard
import com.toptal.caloriecounter.isPasswordValid
import com.toptal.caloriecounter.viewmodel.MainViewModel
import com.toptal.caloriecounter.viewmodel.ProfileViewModel
import com.toptal.caloriecounter.withArgs
import kotlinx.android.synthetic.main.fragment_profile.*
import javax.inject.Inject

class ProfileFragment : BaseFragment() {
    @Inject
    lateinit var moshi: Moshi

    private val viewModel by lazy { provideViewModel<ProfileViewModel>() }
    private val mainViewModel by lazy { provideViewModel<MainViewModel>(activity!!) }

    override fun provideLayoutId(): Int = R.layout.fragment_profile

    override fun provideToolbar(): Toolbar? {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        return toolbar
    }

    override fun initUI(savedInstanceState: Bundle?) {
        email.isEnabled = false

        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        bindProgressButton(updateButton)
        updateButton.attachTextChangeAnimator()
        updateButton.setOnClickListener {
            var valid = true

            if (password.text.toString().isNotEmpty()) {
                if (!isPasswordValid(password.text.toString())) {
                    passwordContainer.error = resources.getString(R.string.auth_password_error)
                    valid = false
                } else {
                    passwordContainer.error = null
                }

                if (password.text.toString() != confirmPassword.text.toString()) {
                    confirmPasswordContainer.error =
                        resources.getString(R.string.auth_confirm_password_error)
                    valid = false
                } else {
                    confirmPasswordContainer.error = null
                }
            }

            if (name.text.toString().isEmpty()) {
                nameContainer.error =
                    resources.getString(R.string.auth_name_error)
                valid = false
            } else {
                nameContainer.error = null
            }

            if (goal.text.toString().isEmpty()) {
                goalContainer.error =
                    resources.getString(R.string.profile_goal_error)
                valid = false
            } else {
                nameContainer.error = null
            }

            if (valid) {
                updateButton.showProgress {
                    buttonTextRes = R.string.edit_meal_progress_text
                    progressColor = R.color.nord0
                }

                hideKeyboard()
                viewModel.onUpdateClicked(
                    name.text.toString(),
                    goal.text.toString().toInt(),
                    password.text.toString()
                )
            }
        }

        bindProgressButton(logoutButton)
        logoutButton.attachTextChangeAnimator()
        logoutButton.setOnClickListener {
            logoutButton.showProgress {
                buttonTextRes = R.string.profile_logout_progress_text
                progressColor = R.color.nord0
            }
            mainViewModel.onLogoutButtonClicked()
        }
    }

    override fun initViewModel(savedInstanceState: Bundle?) {
        viewModel.userSource.observe(viewLifecycleOwner) {
            if (it != null) {
                email.setText(it.email)
                name.setText(it.name)
                goal.setText(it.goal.toString())

                if (it.prettyRole() != Role.Regular) {
                    goalContainer.visibility = View.GONE
                }
            }
        }

        viewModel.userUpdateResult.observe(viewLifecycleOwner) {
            updateButton.hideProgress(R.string.profile_update_button)
            if (it is StatusSuccess) {
                Snackbar.make(
                    view!!,
                    getString(R.string.edit_result_success_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                Snackbar.make(
                    view!!,
                    getString(R.string.edit_result_error_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        mainViewModel.logoutStatus.observe(viewLifecycleOwner) {
            logoutButton.hideProgress(R.string.profile_logout_button)
            if (it is StatusError) {
                Snackbar.make(
                    view!!,
                    getString(R.string.edit_result_error_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }
}