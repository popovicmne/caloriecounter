package com.toptal.caloriecounter.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.toptal.caloriecounter.entity.Meal

@Dao
interface MealDao : BaseDao<Meal> {
    @Query("SELECT * FROM meal WHERE userId = :userId ORDER BY date DESC")
    fun loadAllByUserId(userId: String): DataSource.Factory<Int, Meal>

    @Query("DELETE FROM meal")
    fun clearAll()

    @Query("SELECT _id FROM user")
    fun getLoggedInUserId(): String?
}