package com.toptal.caloriecounter.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.squareup.moshi.Moshi
import com.toptal.caloriecounter.api.AuthService
import com.toptal.caloriecounter.api.MealService
import com.toptal.caloriecounter.api.SessionHelper
import com.toptal.caloriecounter.api.UserService
import com.toptal.caloriecounter.db.CalorieCounterDatabase
import com.toptal.caloriecounter.db.MoshiProvider
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule {
    @Provides
    fun provideSharedPrefs(app: Application): SharedPreferences =
        app.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE)

    @Provides
    fun provideMoshi(): Moshi = MoshiProvider.moshi

    @Singleton
    @Provides
    fun provideDatabase(app: Application): CalorieCounterDatabase {
        return Room.databaseBuilder(app, CalorieCounterDatabase::class.java, "caloriecounter.db").build()
    }

    @Provides
    fun provideMealsDao(db: CalorieCounterDatabase) = db.mealsDao()

    @Provides
    fun provideUserDao(db: CalorieCounterDatabase) = db.userDao()

    @Singleton
    @Provides
    fun provideOkHttpClient(sessionHelper: SessionHelper): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .addInterceptor { chain ->
                var request = chain.request()

                val session = sessionHelper.loadSession()
                if (session.isNotEmpty()) {

                    request = request.newBuilder()
                        .addHeader("Cookie", session)
                        .build()
                }

                val response = chain.proceed(request)

                val cookie = response.header("Set-Cookie", "")!!
                if (cookie.isNotEmpty()) {
                    sessionHelper.saveSession(cookie)
                }

                response
            }
            .connectTimeout(60, TimeUnit.SECONDS)
            .callTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(BASE_URL)
            .build()
    }

    @Singleton
    @Provides
    fun provideAuthService(retrofit: Retrofit): AuthService =
        retrofit.create(AuthService::class.java)

    @Singleton
    @Provides
    fun provideUserService(retrofit: Retrofit): UserService =
        retrofit.create(UserService::class.java)

    @Singleton
    @Provides
    fun provideMealService(retrofit: Retrofit): MealService =
        retrofit.create(MealService::class.java)

    companion object {
        const val BASE_URL = "https://caloriecounter-toptal-test.herokuapp.com"
        const val PREFERENCE_KEY = "preference.shared"
    }
}