package com.toptal.caloriecounter.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.entity.Meal
import com.toptal.caloriecounter.inflate
import kotlinx.android.synthetic.main.view_item_meal.*
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle

class FilterAdapter : RecyclerView.Adapter<BindableViewHolder>() {
    var meals: List<Meal> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindableViewHolder {
        return MealViewHolder(parent.inflate(R.layout.view_item_meal))
    }

    override fun getItemCount(): Int = meals.size

    override fun onBindViewHolder(holder: BindableViewHolder, position: Int) = holder.bind()

    inner class MealViewHolder(containerView: View) : BindableViewHolder(containerView) {
        override fun bind() {
            val meal = meals[adapterPosition]

            text.text = meal.text
            calories.text = meal.kcal.toString() + " kcal"
            time.text = meal.date.atZoneSameInstant(ZoneId.systemDefault())
                .format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT))
        }
    }
}