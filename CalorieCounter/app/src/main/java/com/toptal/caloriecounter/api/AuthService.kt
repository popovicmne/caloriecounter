package com.toptal.caloriecounter.api

import com.toptal.caloriecounter.entity.User
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthService {
    @POST("/login")
    suspend fun login(@Body request: LoginRequest): User

    @GET("/logout")
    suspend fun logout()
}