package com.toptal.caloriecounter.ui.fragment

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.observe
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.Moshi
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.db.MoshiProvider
import com.toptal.caloriecounter.entity.ResourceSuccess
import com.toptal.caloriecounter.entity.StatusSuccess
import com.toptal.caloriecounter.entity.User
import com.toptal.caloriecounter.ui.adapter.UsersAdapter
import com.toptal.caloriecounter.viewmodel.MainViewModel
import com.toptal.caloriecounter.viewmodel.UsersViewModel
import com.toptal.caloriecounter.withArgs
import kotlinx.android.synthetic.main.fragment_users.*
import javax.inject.Inject

class UsersFragment : BaseFragment() {
    @Inject
    lateinit var moshi: Moshi

    private val viewModel by lazy { provideViewModel<UsersViewModel>() }
    private val mainViewModel by lazy { provideViewModel<MainViewModel>(activity!!) }

    private val adapter by lazy {
        val user = arguments?.getString(USER_KEY)?.let {
            moshi.adapter(User::class.java).fromJson(it)
        }!!
        UsersAdapter(user, object : UsersAdapter.Callback {
            override fun onMealsClicked(user: User) {
                mainViewModel.onIconMealsClicked(user)
            }

            override fun onDeleteClicked(user: User) {
                viewModel.onIconDeleteClicked(user)
            }
        })
    }

    override fun provideToolbar(): Toolbar? = toolbar

    override fun provideLayoutId(): Int = R.layout.fragment_users

    override fun initUI(savedInstanceState: Bundle?) {
        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        iconProfile.setOnClickListener {
            mainViewModel.onIconProfileClicked()
        }

        usersRecycler.adapter = adapter
        usersRecycler.layoutManager = LinearLayoutManager(activity!!)
        usersRecycler.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun initViewModel(savedInstanceState: Bundle?) {
        viewModel.users.observe(viewLifecycleOwner) {
            if (it is ResourceSuccess) {
                adapter.users = it.data.toMutableList()
            } else {
                Snackbar.make(
                    view!!,
                    R.string.users_fetch_error,
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        viewModel.userDeleteStatus.observe(viewLifecycleOwner) {
            if (it is ResourceSuccess) {
                adapter.removeUser(it.data)
                Snackbar.make(
                    view!!,
                    getString(R.string.edit_result_success_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                Snackbar.make(
                    view!!,
                    getString(R.string.edit_result_error_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }

    companion object {
        private const val USER_KEY = "users.keys.user"

        fun newInstance(user: User): UsersFragment = UsersFragment().withArgs {
            putString(USER_KEY, MoshiProvider.moshi.adapter(User::class.java).toJson(user))
        }
    }
}