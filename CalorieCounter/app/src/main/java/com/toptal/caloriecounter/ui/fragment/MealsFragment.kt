package com.toptal.caloriecounter.ui.fragment

import android.content.DialogInterface
import android.os.Bundle
import android.os.Parcel
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.Moshi
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.db.MoshiProvider
import com.toptal.caloriecounter.entity.ResourceError
import com.toptal.caloriecounter.entity.ResourceSuccess
import com.toptal.caloriecounter.entity.StatusError
import com.toptal.caloriecounter.entity.User
import com.toptal.caloriecounter.ui.MealsDecoration
import com.toptal.caloriecounter.ui.adapter.MealsAdapter
import com.toptal.caloriecounter.viewmodel.MainViewModel
import com.toptal.caloriecounter.viewmodel.MealsViewModel
import com.toptal.caloriecounter.withArgs
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import com.wdullaer.materialdatetimepicker.time.Timepoint
import kotlinx.android.synthetic.main.fragment_meals.*
import org.threeten.bp.OffsetDateTime
import javax.inject.Inject

class MealsFragment : BaseFragment() {
    @Inject
    lateinit var moshi: Moshi

    private val viewModel by lazy { provideViewModel<MealsViewModel>() }
    private val mainViewModel by lazy { provideViewModel<MainViewModel>(activity!!) }

    private val adapter by lazy {
        MealsAdapter { meal ->
            MaterialAlertDialogBuilder(activity!!)
                .setTitle(meal.text)
                .setPositiveButton(R.string.meals_dialog_update_button) { _: DialogInterface, _: Int ->
                    mainViewModel.onMealUpdateClicked(meal)
                }
                .setNegativeButton(R.string.meals_dialog_delete_button) { _: DialogInterface, _: Int ->
                    viewModel.onMealDeleteClicked(meal)
                }
                .show()
        }
    }
    private var dateFrom: Long = -1
    private var dateTo: Long = -1
    private var timeFrom: Int = -1
    private var timeTo: Int = -1

    override fun provideLayoutId(): Int = R.layout.fragment_meals
    override fun provideToolbar(): Toolbar? = toolbar

    override fun initUI(savedInstanceState: Bundle?) {
        mealsRecycler.adapter = adapter
        mealsRecycler.layoutManager = LinearLayoutManager(activity!!)
        mealsRecycler.addItemDecoration(MealsDecoration(activity!!))
        mealsRecycler.itemAnimator = null

        iconProfile.setOnClickListener {
            mainViewModel.onIconProfileClicked()
        }

        iconCreate.setOnClickListener {
            mainViewModel.onIconCreateClicked()
        }

        iconFilter.setOnClickListener {
            val currentMillis = OffsetDateTime.now().toInstant().toEpochMilli()
            val calendarConstraints = CalendarConstraints.Builder()
                .setEnd(currentMillis)
                .setValidator(object : CalendarConstraints.DateValidator {
                    override fun writeToParcel(dest: Parcel?, flags: Int) {}

                    override fun isValid(date: Long): Boolean {
                        return date <= currentMillis
                    }

                    override fun describeContents(): Int = 0
                })
                .build()


            MaterialDatePicker.Builder.dateRangePicker()
                .setTheme(R.style.MaterialCalendarAppearance)
                .setCalendarConstraints(calendarConstraints)
                .build()
                .apply {
                    addOnPositiveButtonClickListener {
                        dateFrom = it.first!!
                        dateTo = it.second!!
                        showTimePicker {
                            timeFrom = it
                            showTimePicker {
                                timeTo = it
                                mainViewModel.onFilterByDateTime(dateFrom, dateTo, timeFrom, timeTo)
                            }
                        }
                    }
                }.show(childFragmentManager, null)
        }

        swipeToRefresh.setOnRefreshListener {
            viewModel.onRefresh()
        }
    }

    private fun showTimePicker(callback: (hour: Int) -> Unit) {
        val dialog =
            TimePickerDialog.newInstance({ _: TimePickerDialog, hour: Int, _: Int, _: Int ->
                callback(hour)
            }, false)
        dialog.isThemeDark = true
        dialog.apply {
            enableMinutes(false)
            enableSeconds(false)
        }.show(activity!!.supportFragmentManager, null)
    }

    override fun initViewModel(savedInstanceState: Bundle?) {
        viewModel.meals.observe(viewLifecycleOwner) {
            adapter.submitList(it)
            mealsRecycler.smoothScrollToPosition(0)
        }

        viewModel.user.observe(viewLifecycleOwner) { user ->
            user?.let {
                adapter.setUserGoal(it.goal)
            }
        }

        viewModel.mealDeleteResult.observe(viewLifecycleOwner) {
            if (it is ResourceSuccess) {
                Snackbar.make(
                    view!!,
                    getString(R.string.meals_delete_result_success_message, it.data.text),
                    Snackbar.LENGTH_SHORT
                ).show()
            } else if (it is ResourceError) {
                Snackbar.make(
                    view!!,
                    getString(R.string.meals_delete_result_error_message, it.data?.text),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        viewModel.mealsFetchStatus.observe(viewLifecycleOwner) {
            if (it is StatusError) {
                Snackbar.make(
                    view!!,
                    getString(R.string.edit_result_error_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        viewModel.refreshStatus.observe(viewLifecycleOwner) {
            swipeToRefresh.isRefreshing = it
        }

        val user = arguments?.getString(USER_KEY)?.let {
            moshi.adapter(User::class.java).fromJson(it)
        }
        if (user != null) {
            toolbar.title = user.email
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
            toolbar.setNavigationOnClickListener {
                activity?.onBackPressed()
            }
            iconProfile.visibility = View.GONE
            iconFilter.visibility = View.GONE
            iconCreate.visibility = View.GONE
        }
        viewModel.init(user)
    }

    companion object {
        private const val USER_KEY = "meals.keys.user"

        fun newInstance(user: User? = null): MealsFragment = MealsFragment().withArgs {
            putString(USER_KEY, MoshiProvider.moshi.adapter(User::class.java).toJson(user))
        }
    }
}