package com.toptal.caloriecounter.entity

import java.lang.Exception

sealed class Resource<out T>

open class ResourceSuccess<out T>(val data: T) : Resource<T>()
class ResourceLoading<out T> : Resource<T>()
open class ResourceError<out T>(val exception: Exception? = null, val code: Int = 0, val data: T? = null) : Resource<T>()

typealias ResourceStatus = Resource<Unit>
object StatusSuccess : ResourceSuccess<Unit>(Unit)
object StatusError : ResourceError<Unit>()