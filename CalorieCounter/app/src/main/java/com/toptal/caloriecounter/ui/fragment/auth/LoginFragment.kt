package com.toptal.caloriecounter.ui.fragment.auth

import android.os.Bundle
import androidx.lifecycle.observe
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.hideKeyboard
import com.toptal.caloriecounter.isEmailValid
import com.toptal.caloriecounter.isPasswordValid
import com.toptal.caloriecounter.ui.fragment.BaseFragment
import com.toptal.caloriecounter.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment() {
    private val viewModel by lazy { provideViewModel<AuthViewModel>(parentFragment!!) }

    override fun provideLayoutId(): Int = R.layout.fragment_login

    override fun initUI(savedInstanceState: Bundle?) {
        clickToSignUpText.setOnClickListener {
            viewModel.onClickHereToSignUpTextClicked(
                email.text.toString(),
                password.text.toString()
            )
        }

        bindProgressButton(loginButton)
        loginButton.attachTextChangeAnimator()
        loginButton.setOnClickListener {
            var valid = true
            if (!isEmailValid(email.text.toString())) {
                emailContainer.error = resources.getString(R.string.auth_email_error)
                valid = false
            } else {
                emailContainer.error = null
            }

            if (!isPasswordValid(password.text.toString())) {
                passwordContainer.error = resources.getString(R.string.auth_password_error)
                valid = false
            } else {
                passwordContainer.error = null
            }

            if (valid) {
                hideKeyboard()
                loginButton.showProgress {
                    buttonTextRes = R.string.auth_login_progress
                    progressColor = R.color.nord0
                }
                viewModel.onLoginClicked(email.text.toString(), password.text.toString())
            }
        }
    }

    override fun initViewModel(savedInstanceState: Bundle?) {
        viewModel.user.observe(viewLifecycleOwner) {
            if (it != null) {
                email.setText(it.email)
                password.setText(it.password)
            }
        }

        viewModel.authResult.observe(viewLifecycleOwner) {
            loginButton.hideProgress(R.string.auth_login_button)
        }
    }
}