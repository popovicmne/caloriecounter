package com.toptal.caloriecounter.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.hadilq.liveevent.LiveEvent
import com.toptal.caloriecounter.entity.*
import com.toptal.caloriecounter.repository.AuthRepository
import com.toptal.caloriecounter.repository.UserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class UsersViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {
    val users: LiveData<Resource<List<User>>> = liveData {
        emit(userRepository.getUsers())
    }
    val userDeleteStatus = LiveEvent<Resource<User>>()

    fun onIconDeleteClicked(user: User) = viewModelScope.launch {
        val status = userRepository.delete(user)
        if (status is StatusSuccess) {
            userDeleteStatus.value = ResourceSuccess(user)
        } else {
            userDeleteStatus.value = ResourceError()
        }
    }
}