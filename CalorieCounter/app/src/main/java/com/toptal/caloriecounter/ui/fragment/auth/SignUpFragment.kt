package com.toptal.caloriecounter.ui.fragment.auth

import android.os.Bundle
import androidx.lifecycle.observe
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.hideKeyboard
import com.toptal.caloriecounter.isEmailValid
import com.toptal.caloriecounter.isPasswordValid
import com.toptal.caloriecounter.ui.fragment.BaseFragment
import com.toptal.caloriecounter.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.fragment_signup.*

class SignUpFragment : BaseFragment() {
    private val viewModel by lazy { provideViewModel<AuthViewModel>(parentFragment!!) }

    override fun provideLayoutId(): Int = R.layout.fragment_signup

    override fun initUI(savedInstanceState: Bundle?) {
        clickToLoginText.setOnClickListener {
            viewModel.onClickHereToLoginTextClicked(email.text.toString(), password.text.toString())
        }

        signupButton.setOnClickListener {
            var valid = true
            if (!isEmailValid(email.text.toString())) {
                emailContainer.error = resources.getString(R.string.auth_email_error)
                valid = false
            } else {
                emailContainer.error = null
            }

            if (!isPasswordValid(password.text.toString())) {
                passwordContainer.error = resources.getString(R.string.auth_password_error)
                valid = false
            } else {
                passwordContainer.error = null
            }

            if (password.text.toString() != confirmPassword.text.toString()) {
                confirmPasswordContainer.error =
                    resources.getString(R.string.auth_confirm_password_error)
                valid = false
            } else {
                confirmPasswordContainer.error = null
            }

            if (name.text.toString().isEmpty()) {
                nameContainer.error =
                    resources.getString(R.string.auth_name_error)
                valid = false
            } else {
                nameContainer.error = null
            }

            if (valid) {
                hideKeyboard()
                signupButton.showProgress {
                    buttonTextRes = R.string.auth_signup_process
                    progressColor = R.color.nord0
                }
                viewModel.onSignUpClicked(
                    email.text.toString(),
                    password.text.toString(),
                    name.text.toString()
                )
            }
        }

    }

    override fun initViewModel(savedInstanceState: Bundle?) {
        viewModel.user.observe(viewLifecycleOwner) {
            if (it != null) {
                email.setText(it.email)
                password.setText(it.password)
                name.setText(it.name)
            }
        }

        viewModel.authResult.observe(viewLifecycleOwner) {
            signupButton.hideProgress(R.string.auth_signup_button)
        }
    }
}