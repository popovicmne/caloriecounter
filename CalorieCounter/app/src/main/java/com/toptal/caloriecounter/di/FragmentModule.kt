package com.toptal.caloriecounter.di

import com.toptal.caloriecounter.ui.fragment.*
import com.toptal.caloriecounter.ui.fragment.auth.AuthFragment
import com.toptal.caloriecounter.ui.fragment.auth.LoginFragment
import com.toptal.caloriecounter.ui.fragment.auth.SignUpFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeMealsFragment(): MealsFragment

    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeSignupFragment(): SignUpFragment

    @ContributesAndroidInjector
    abstract fun contributeAuthFragment(): AuthFragment

    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashFragment

    @ContributesAndroidInjector
    abstract fun contributeEditMealFragment(): EditMealFragment

    @ContributesAndroidInjector
    abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeUsersFragment(): UsersFragment

    @ContributesAndroidInjector
    abstract fun contributeFilterFragment(): FilterFragment
}