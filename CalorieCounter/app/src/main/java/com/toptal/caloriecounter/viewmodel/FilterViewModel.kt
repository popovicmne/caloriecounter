package com.toptal.caloriecounter.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.toptal.caloriecounter.entity.FilterRequest
import com.toptal.caloriecounter.repository.MealRepository
import javax.inject.Inject

class FilterViewModel @Inject constructor(
    private val mealRepository: MealRepository
) : ViewModel() {
    private val filterRequest = MutableLiveData<FilterRequest>()
    val meals = filterRequest.switchMap {
        liveData {
            emit(mealRepository.filter(it.fromDate, it.toDate, it.fromTime, it.toTime))
        }
    }

    fun init(filterRequest: FilterRequest) {
        this.filterRequest.value = filterRequest
    }
}