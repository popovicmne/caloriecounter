package com.toptal.caloriecounter.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import org.threeten.bp.OffsetDateTime

@JsonClass(generateAdapter = true)
@Entity
data class Meal(
    @PrimaryKey
    val _id: String,
    val text: String,
    val kcal: Int,
    val date: OffsetDateTime,
    val userId: String
)

@JsonClass(generateAdapter = true)
@Entity
data class User(
    @PrimaryKey
    val _id: String,
    val email: String,
    val name: String,
    val goal: Int,
    val role: Int = 0
)

fun User.prettyRole(): Role {
    return when (role) {
        1 -> Role.UserManager
        2 -> Role.Admin
        else -> Role.Regular
    }
}

sealed class Role {
    object Regular : Role()
    object UserManager : Role()
    object Admin : Role()
}

@JsonClass(generateAdapter = true)
data class FilterRequest(val fromDate: Long, val toDate: Long, val fromTime: Int, val toTime: Int)