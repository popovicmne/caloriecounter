package com.toptal.caloriecounter.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.toptal.caloriecounter.entity.Meal
import com.toptal.caloriecounter.entity.User

@Database(
    entities = [User::class, Meal::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(OffsetDateTimeTypeConverter::class)
abstract class CalorieCounterDatabase : RoomDatabase() {
    abstract fun mealsDao(): MealDao
    abstract fun userDao(): UserDao
}