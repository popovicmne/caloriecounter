package com.toptal.caloriecounter.ui.fragment

import com.toptal.caloriecounter.R

class SplashFragment : BaseFragment() {
    override fun provideLayoutId(): Int = R.layout.fragment_splash
}