package com.toptal.caloriecounter.viewmodel

import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hadilq.liveevent.LiveEvent
import com.toptal.caloriecounter.entity.*
import com.toptal.caloriecounter.repository.AuthRepository
import com.toptal.caloriecounter.repository.UserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val userRepository: UserRepository
) : ViewModel() {
    val screen = LiveEvent<Screen>()
    val authResult = LiveEvent<User>()
    val logoutStatus = LiveEvent<ResourceStatus>()
    val user = LiveEvent<User>()

    fun init(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            viewModelScope.launch {
                user.value = authRepository.loadLoggedIn()
            }
        }
    }

    fun onIconCreateClicked() {
        screen.value = Screen.EditMealScreen()
    }

    fun onMealUpdateClicked(meal: Meal) {
        screen.value = Screen.EditMealScreen(meal)
    }

    fun onIconProfileClicked() {
        screen.value = Screen.ProfileScreen
    }

    fun onIconMealsClicked(user: User) {
        screen.value = Screen.MealsScreen(user)
    }

    fun onLogoutButtonClicked() = viewModelScope.launch {
        val status = authRepository.logout()
        logoutStatus.value = status
        if (status is StatusSuccess) {
            screen.value = Screen.AuthScreen
        }
    }

    fun onFilterByDateTime(fromDate: Long, toDate: Long, fromTime: Int, toTime: Int) {
        screen.value = Screen.FilterScreen(FilterRequest(fromDate, toDate, fromTime, toTime))
    }
}

sealed class Screen {
    object SplashScreen : Screen()
    object AuthScreen : Screen()
    data class MealsScreen(val user: User? = null) : Screen()
    data class EditMealScreen(val meal: Meal? = null) : Screen()
    object ProfileScreen : Screen()
    data class FilterScreen(val filterRequest: FilterRequest) : Screen()
}