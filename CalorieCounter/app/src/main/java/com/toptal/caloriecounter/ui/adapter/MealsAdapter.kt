package com.toptal.caloriecounter.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.entity.Meal
import kotlinx.android.synthetic.main.view_item_meal.*
import kotlinx.android.synthetic.main.view_item_meal_date.*
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle

class MealsAdapter(private val callback: (meal: Meal) -> Unit) : PagedListAdapter<Meal, MealsAdapter.MealViewHolder>(DIFF_CALLBACK) {
    private var dayMap: Map<LocalDate, Boolean> = HashMap()
    private var userGoal: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealViewHolder {
        return when(viewType) {
            VIEW_TYPE_FIRST_IN_DAY,
            VIEW_TYPE_SINGLE_IN_DAY -> {
                MealDateViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.view_item_meal_date,
                        parent,
                        false
                    )
                )
            }
            else -> {
                MealViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.view_item_meal,
                        parent,
                        false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: MealViewHolder, position: Int) = holder.bind()

    override fun getItemViewType(position: Int): Int {
        val currentDate = getItem(position)!!.date.toLocalDate()
        if (position == 0) {
            return if (itemCount == 1) {
                VIEW_TYPE_SINGLE_IN_DAY
            } else {
                val nextDate = getItem(position + 1)!!.date.toLocalDate()
                if (currentDate.isEqual(nextDate)) {
                    VIEW_TYPE_FIRST_IN_DAY
                } else {
                    VIEW_TYPE_SINGLE_IN_DAY
                }
            }
        }

        val previousDate = getItem(position - 1)!!.date.toLocalDate()

        return if (currentDate.isEqual(previousDate)) {
            if (position == itemCount - 1) {
                VIEW_TYPE_LAST_IN_DAY
            } else {
                val nextDate = getItem(position + 1)!!.date.toLocalDate()
                if (currentDate.isEqual(nextDate)) {
                    VIEW_TYPE_EQUAL_IN_DAY
                } else {
                    VIEW_TYPE_LAST_IN_DAY
                }
            }
        } else {
            if (position == itemCount - 1) {
                VIEW_TYPE_SINGLE_IN_DAY
            } else {
                val nextDate = getItem(position + 1)!!.date.toLocalDate()
                if (currentDate.isEqual(nextDate)) {
                    VIEW_TYPE_FIRST_IN_DAY
                } else {
                    VIEW_TYPE_SINGLE_IN_DAY
                }
            }
        }
    }

    override fun submitList(pagedList: PagedList<Meal>?) {
        super.submitList(pagedList) {
            currentList?.let { calculateGoalMap(it) }
        }
    }

    fun setUserGoal(goal: Int) {
        if (goal == userGoal) return

        userGoal = goal

        val meals = ArrayList<Meal>()
        for (i in 0 until itemCount) {
            meals.add(getItem(i)!!)
        }

        calculateGoalMap(meals)

        notifyDataSetChanged()
    }

    private fun calculateGoalMap(meals: List<Meal>) {
        dayMap = meals.groupBy {
            it.date.toLocalDate()
        }.mapValues { map ->
            var isUnder = true

            var calories = 0
            map.value.forEach { meal ->
                calories += meal.kcal
            }

            if (calories > userGoal) isUnder = false

            isUnder
        }
    }

    fun isUnder(position: Int): Boolean = dayMap[getItem(position)!!.date.toLocalDate()]!!

    open inner class MealViewHolder(containerView: View) : BindableViewHolder(containerView) {

        init {
            itemRoot.setOnLongClickListener {
                callback(getItem(adapterPosition)!!)
                true
            }
        }

        override fun bind() {
            val meal = getItem(adapterPosition)!!

            text.text = meal.text
            calories.text = meal.kcal.toString() + " kcal"
            time.text = meal.date.atZoneSameInstant(ZoneId.systemDefault())
                .format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT))
        }
    }

    inner class MealDateViewHolder(containerView: View) : MealViewHolder(containerView) {
        override fun bind() {
            super.bind()

            val meal = getItem(adapterPosition)!!

            date.text = meal.date.atZoneSameInstant(ZoneId.systemDefault()).format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG))
        }
    }

    companion object {
        const val VIEW_TYPE_EQUAL_IN_DAY = 0
        const val VIEW_TYPE_FIRST_IN_DAY = 1
        const val VIEW_TYPE_LAST_IN_DAY = 2
        const val VIEW_TYPE_SINGLE_IN_DAY = 3

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Meal>() {
            override fun areContentsTheSame(oldItem: Meal, newItem: Meal): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(oldItem: Meal, newItem: Meal): Boolean =
                oldItem._id == newItem._id
        }
    }
}