package com.toptal.caloriecounter.api

import com.toptal.caloriecounter.entity.Meal
import retrofit2.http.*

interface MealService {
    @GET("/meal")
    suspend fun getAll(@Query("userId") userId: String? = null, @Query("id") nextPage: String? = null): List<Meal>

    @GET("/meal/{id}")
    suspend fun get(@Path("id") id: String): Meal

    @POST("/meal")
    suspend fun create(@Body createMealRequest: CreateMealRequest): Meal

    @PUT("/meal/{id}")
    suspend fun update(@Path("id") id: String, @Body editMealRequest: EditMealRequest)

    @DELETE("/meal/{id}")
    suspend fun delete(@Path("id") id: String)

    @GET("meal/filter")
    suspend fun filter(
        @Query("fromDate") fromDate: String,
        @Query("toDate") toDate: String,
        @Query("fromTime") fromTime: Int,
        @Query("toTime") toTime: Int
    ): List<Meal>
}