package com.toptal.caloriecounter.ui

import android.widget.TextView
import androidx.fragment.app.commit
import androidx.transition.TransitionInflater
import androidx.transition.TransitionSet
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.entity.User
import com.toptal.caloriecounter.ui.fragment.MealsFragment
import com.toptal.caloriecounter.ui.fragment.UsersFragment
import com.toptal.caloriecounter.ui.fragment.auth.AuthFragment
import java.util.concurrent.TimeUnit

fun MainActivity.transitionFromSplashToAuth() {
    val duration = 1000L

    val nextFragment = AuthFragment()
    val enterTransition = TransitionInflater.from(this).inflateTransition(android.R.transition.fade)
    enterTransition.startDelay = duration
    enterTransition.duration = 1000
    nextFragment.enterTransition = enterTransition

    val enterTransitionSet = TransitionSet()
    enterTransitionSet.addTransition(
        TransitionInflater.from(this).inflateTransition(
            android.R.transition.move
        )
    )
    enterTransitionSet.duration = duration
    nextFragment.sharedElementEnterTransition = enterTransitionSet

    val splashFragment = supportFragmentManager.findFragmentById(R.id.container)
    val title = splashFragment?.view?.findViewById<TextView>(R.id.title)

    nextFragment.postponeEnterTransition(1, TimeUnit.SECONDS)

    supportFragmentManager.commit(true) {
        setReorderingAllowed(true)
        addSharedElement(title!!, title.transitionName)
        replace(
            R.id.container,
            nextFragment
        )
    }
}

fun MainActivity.transitionFromSplashToMeals() {
    val splashFragment = supportFragmentManager.findFragmentById(R.id.container)

    supportFragmentManager.commit {
        replace(R.id.container, MealsFragment())
    }
}

fun MainActivity.transitionFromSplashToUsers(user: User) {
    val splashFragment = supportFragmentManager.findFragmentById(R.id.container)

    supportFragmentManager.commit {
        replace(R.id.container, UsersFragment.newInstance(user))
    }
}