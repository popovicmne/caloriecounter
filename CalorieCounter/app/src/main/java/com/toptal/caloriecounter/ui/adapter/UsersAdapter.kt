package com.toptal.caloriecounter.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.entity.Role
import com.toptal.caloriecounter.entity.User
import com.toptal.caloriecounter.entity.prettyRole
import com.toptal.caloriecounter.inflate
import kotlinx.android.synthetic.main.view_item_user.*

class UsersAdapter(private val user: User, private val callback: Callback) :
    RecyclerView.Adapter<BindableViewHolder>() {
    var users: MutableList<User> = ArrayList()
        set(value) {
            field = value.filter { it.role < user.role }.toMutableList()
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindableViewHolder {
        return UserViewHolder(parent.inflate(R.layout.view_item_user))
    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: BindableViewHolder, position: Int) = holder.bind()

    inner class UserViewHolder(containerView: View) : BindableViewHolder(containerView) {
        init {
            iconDelete.setOnClickListener {
                callback.onDeleteClicked(users[adapterPosition])
            }

            iconMeals.setOnClickListener {
                callback.onMealsClicked(users[adapterPosition])
            }
        }

        override fun bind() {
            val user = users[adapterPosition]

            if (user.prettyRole() != Role.Regular || this@UsersAdapter.user.prettyRole() != Role.Admin) {
                iconMeals.visibility = View.GONE
            } else {
                iconMeals.visibility = View.VISIBLE
            }
            email.text =
                containerView.resources.getString(R.string.users_item_email_text, user.email)
            name.text = containerView.resources.getString(R.string.users_item_name_text, user.name)
        }
    }

    fun removeUser(user: User) {
        users.remove(user)
        notifyDataSetChanged()
    }

    interface Callback {
        fun onMealsClicked(user: User)
        fun onDeleteClicked(user: User)
    }
}