package com.toptal.caloriecounter.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hadilq.liveevent.LiveEvent
import com.toptal.caloriecounter.entity.*
import com.toptal.caloriecounter.repository.MealRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class EditMealViewModel @Inject constructor(
    private val mealRepository: MealRepository
) : ViewModel() {
    val editStatus = LiveEvent<ResourceStatus>()

    private var meal: Meal? = null

    fun init(meal: Meal) {
        this.meal = meal
    }

    fun onEditButtonClicked(text: String, kcal: Int) {
        if (meal != null) {
            updateMeal(text, kcal)
        } else {
            createMeal(text, kcal)
        }
    }

    private fun updateMeal(text: String, kcal: Int) {
        meal?.copy(text = text, kcal = kcal)?.let {
            viewModelScope.launch {
                editStatus.postValue(mealRepository.update(it))
            }
        }
    }

    private fun createMeal(text: String, kcal: Int) = viewModelScope.launch {
        val resource = mealRepository.create(text, kcal)
        if (resource is ResourceSuccess) {
            editStatus.postValue(StatusSuccess)
        } else {
            editStatus.postValue(StatusError)
        }
    }
}