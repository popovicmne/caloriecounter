package com.toptal.caloriecounter.repository

import androidx.lifecycle.LiveData
import com.toptal.caloriecounter.api.*
import com.toptal.caloriecounter.db.CalorieCounterDatabase
import com.toptal.caloriecounter.db.UserDao
import com.toptal.caloriecounter.entity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import javax.inject.Inject


class AuthRepository @Inject constructor(
    private val db: CalorieCounterDatabase,
    private val authService: AuthService,
    private val userService: UserService,
    private val userDao: UserDao,
    private val sessionHelper: SessionHelper
) {
    fun loadSession(): String = sessionHelper.loadSession()

    fun loadLoggedInAsSource(): LiveData<User> = userDao.loadAsSource()

    suspend fun loadLoggedIn() = withContext(Dispatchers.IO) {
        userDao.load()
    }

    suspend fun logout() = withContext(Dispatchers.IO) {
        try {
            authService.logout()
            sessionHelper.clearSession()
            db.clearAllTables()
            StatusSuccess
        } catch (e: Exception) {
            StatusError
        }
    }

    suspend fun login(email: String, password: String) =
        withContext<Resource<User>>(Dispatchers.IO) {
            try {
                val user = authService.login(LoginRequest(email, password))
                userDao.insert(user)
                ResourceSuccess(user)
            } catch (e: Exception) {
                ResourceError(e)
            }
        }

    suspend fun signUp(email: String, password: String, name: String) =
        withContext<Resource<User>>(Dispatchers.IO) {
            try {
                val user = userService.create(SignUpRequest(email, password, name))
                userDao.insert(user)
                ResourceSuccess(user)
            } catch (e: Exception) {
                ResourceError(e)
            }
        }
}