package com.toptal.caloriecounter.ui.fragment

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.observe
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.Moshi
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.db.MoshiProvider
import com.toptal.caloriecounter.entity.FilterRequest
import com.toptal.caloriecounter.entity.ResourceSuccess
import com.toptal.caloriecounter.ui.adapter.FilterAdapter
import com.toptal.caloriecounter.viewmodel.FilterViewModel
import com.toptal.caloriecounter.withArgs
import kotlinx.android.synthetic.main.fragment_filter.*
import org.threeten.bp.Instant
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import javax.inject.Inject

class FilterFragment : BaseFragment() {
    @Inject
    lateinit var moshi: Moshi

    private val viewModel by lazy { provideViewModel<FilterViewModel>() }
    private val adapter by lazy { FilterAdapter() }

    override fun provideToolbar(): Toolbar? {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        return toolbar
    }

    override fun provideLayoutId(): Int = R.layout.fragment_filter

    override fun initUI(savedInstanceState: Bundle?) {
        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        filterRecycler.adapter = adapter
        filterRecycler.layoutManager = LinearLayoutManager(activity!!)
        filterRecycler.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.VERTICAL
            )
        )
    }

    override fun initViewModel(savedInstanceState: Bundle?) {
        viewModel.meals.observe(viewLifecycleOwner) {
            if (it is ResourceSuccess) {
                adapter.meals = it.data
            } else {
                Snackbar.make(
                    view!!,
                    getString(R.string.edit_result_error_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        arguments?.getString(REQUEST_KEY)?.let {
            val filterRequest = moshi.adapter(FilterRequest::class.java).fromJson(it)!!

            val offsetFromDate = OffsetDateTime.ofInstant(
                Instant.ofEpochMilli(filterRequest.fromDate),
                ZoneId.systemDefault()
            ).format(
                DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
            )

            val offsetToDate = OffsetDateTime.ofInstant(
                Instant.ofEpochMilli(filterRequest.toDate),
                ZoneId.systemDefault()
            ).format(
                DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
            )

            toolbar.title =
                resources.getString(R.string.filter_toolbar_title, offsetFromDate, offsetToDate)

            viewModel.init(filterRequest)
        }
    }

    companion object {
        private const val REQUEST_KEY = "filter.keys.request"

        fun newInstance(filterRequest: FilterRequest): FilterFragment = FilterFragment().withArgs {
            putString(
                REQUEST_KEY,
                MoshiProvider.moshi.adapter(FilterRequest::class.java).toJson(filterRequest)
            )
        }
    }
}