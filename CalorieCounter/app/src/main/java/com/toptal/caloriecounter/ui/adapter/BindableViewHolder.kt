package com.toptal.caloriecounter.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

abstract class BindableViewHolder(override val containerView: View): RecyclerView.ViewHolder(containerView),
    LayoutContainer {
    abstract fun bind()
}