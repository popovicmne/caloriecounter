package com.toptal.caloriecounter.api

data class LoginRequest(val email: String, val password: String)
data class SignUpRequest(val email: String, val password: String, val name: String)

data class UpdateUserRequest(val name: String, val goal: Int, val password: String? = null)

data class EditMealRequest(val text: String, val kcal: Int)
data class CreateMealRequest(val text: String, val kcal: Int, val date: String)