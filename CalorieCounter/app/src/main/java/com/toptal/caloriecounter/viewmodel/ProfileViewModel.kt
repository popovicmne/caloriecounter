package com.toptal.caloriecounter.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hadilq.liveevent.LiveEvent
import com.toptal.caloriecounter.entity.ResourceStatus
import com.toptal.caloriecounter.repository.AuthRepository
import com.toptal.caloriecounter.repository.UserRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val authRepository: AuthRepository
) : ViewModel() {
    val userSource = authRepository.loadLoggedInAsSource()
    val userUpdateResult = LiveEvent<ResourceStatus>()

    fun onUpdateClicked(name: String, goal: Int, password: String) = viewModelScope.launch {
        userSource.value?.let {
            var newPassword = if (password.isNotEmpty()) {
                password
            } else {
                null
            }
            val status = userRepository.update(it, name, goal, newPassword)
            userUpdateResult.value = status
        }
    }
}