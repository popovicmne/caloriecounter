package com.toptal.caloriecounter.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hadilq.liveevent.LiveEvent
import com.toptal.caloriecounter.entity.Resource
import com.toptal.caloriecounter.entity.ResourceSuccess
import com.toptal.caloriecounter.entity.User
import com.toptal.caloriecounter.repository.AuthRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class AuthViewModel @Inject constructor(private val authRepository: AuthRepository) : ViewModel() {
    val user = MutableLiveData<UserRequest>()
    val authResult = LiveEvent<Resource<User>>()
    val screen = LiveEvent<Screen>()

    fun onSignUpClicked(email: String, password: String, name: String) = viewModelScope.launch {
        val resource = authRepository.signUp(email, password, name)
        authResult.value = resource
    }

    fun onLoginClicked(email: String, password: String) = viewModelScope.launch {
        val resource = authRepository.login(email, password)
        authResult.value = resource
    }

    fun onClickHereToSignUpTextClicked(email: String, password: String) {
        copyUser(email, password)
        screen.value = Screen.SignUpScreen
    }

    fun onClickHereToLoginTextClicked(email: String, password: String) {
        copyUser(email, password)
        screen.value = Screen.LoginScreen
    }

    private fun copyUser(email: String, password: String) {
        user.value = user.value?.copy(email = email, password = password) ?: UserRequest(email, password, "")
    }

    data class UserRequest(val email: String, val password: String, val name: String)

    sealed class Screen {
        object LoginScreen : Screen()
        object SignUpScreen : Screen()
    }
}

