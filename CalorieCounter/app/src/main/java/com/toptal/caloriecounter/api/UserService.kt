package com.toptal.caloriecounter.api

import com.toptal.caloriecounter.entity.User
import retrofit2.http.*

interface UserService {
    @GET("/user")
    suspend fun getAll(): List<User>

    @GET("/user/{id}")
    suspend fun get(@Path("id") id: String): User

    @POST("/user")
    suspend fun create(@Body signUpRequest: SignUpRequest): User

    @PUT("/user/{id}")
    suspend fun update(@Path("id") id: String, @Body updateUserRequest: UpdateUserRequest): User

    @DELETE("/user/{id}")
    suspend fun delete(@Path("id") id: String)
}