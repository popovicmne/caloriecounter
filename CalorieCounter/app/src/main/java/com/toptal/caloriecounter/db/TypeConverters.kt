package com.toptal.caloriecounter.db

import androidx.room.TypeConverter
import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter

private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

object OffsetDateTimeTypeConverter {
    @TypeConverter
    @JvmStatic
    fun toOffsetDateTime(value: String): OffsetDateTime {
        return formatter.parse(value, OffsetDateTime::from)
    }

    @TypeConverter
    @JvmStatic
    fun fromOffsetDateTime(date: OffsetDateTime): String {
        return date.format(formatter)
    }
}

class OffsetDateTimeMoshiAdapter {
    @ToJson
    fun toJson(date: OffsetDateTime): String {
        return date.format(formatter)
    }

    @FromJson
    fun fromJson(value: String): OffsetDateTime {
        return formatter.parse(value, OffsetDateTime::from)
    }
}

object MoshiProvider {
    val moshi: Moshi = Moshi.Builder().add(OffsetDateTimeMoshiAdapter()).build()
}