package com.toptal.caloriecounter.ui

import android.content.Context
import android.graphics.*
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.ui.adapter.MealsAdapter

class MealsDecoration(context: Context) : RecyclerView.ItemDecoration() {
    private val bounds = Rect()
    private val offset = context.resources.getDimension(R.dimen.meal_day_offset).toInt()
    private val paint = Paint()
    private val backgroundRoundedPaint = Paint()
    private val backgroundPaint = Paint()
    private val dividerPaint = Paint()
    private val path = Path()
    private val dividerPath = Path()
    private val isUnderColor = context.resources.getColor(R.color.nord14)
    private val isOverColor = context.resources.getColor(R.color.nord11)

    init {
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = context.resources.getDimension(R.dimen.meal_outline_stroke_width)
        paint.isAntiAlias = true
        paint.strokeJoin = Paint.Join.ROUND
        paint.pathEffect =
            CornerPathEffect(context.resources.getDimension(R.dimen.meal_outline_corner_radius))

        dividerPaint.style = Paint.Style.STROKE
        dividerPaint.strokeWidth = context.resources.getDimension(R.dimen.meal_divider_stroke_width)
        dividerPaint.isAntiAlias = true
        dividerPaint.color = context.resources.getColor(R.color.nord1)

        backgroundPaint.style = Paint.Style.FILL
        backgroundPaint.strokeWidth = 0f
        backgroundPaint.isAntiAlias = true
        backgroundPaint.color = context.resources.getColor(R.color.nord2)

        backgroundRoundedPaint.style = Paint.Style.FILL
        backgroundRoundedPaint.strokeWidth = 0f
        backgroundRoundedPaint.isAntiAlias = true
        backgroundRoundedPaint.color = context.resources.getColor(R.color.nord2)
        backgroundRoundedPaint.strokeJoin = Paint.Join.ROUND
        backgroundRoundedPaint.pathEffect =
            CornerPathEffect(context.resources.getDimension(R.dimen.meal_outline_corner_radius))
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)

        if (position == RecyclerView.NO_POSITION) return

        when (parent.adapter!!.getItemViewType(position)) {
            MealsAdapter.VIEW_TYPE_LAST_IN_DAY,
            MealsAdapter.VIEW_TYPE_SINGLE_IN_DAY -> {
                outRect.set(0, 0, 0, offset)
            }
            MealsAdapter.VIEW_TYPE_EQUAL_IN_DAY,
            MealsAdapter.VIEW_TYPE_FIRST_IN_DAY -> {
                outRect.set(0, 0, 0, 0)
            }
        }
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        c.save()

        val adapter = parent.adapter as MealsAdapter

        for (i in 0 until parent.childCount) {
            val child = parent.getChildAt(i)
            val position = parent.getChildAdapterPosition(child)

            if (position == RecyclerView.NO_POSITION) return

            val viewType = adapter.getItemViewType(position)

            parent.getDecoratedBoundsWithMargins(child, bounds)
            var top = bounds.top.toFloat()
            var bottom = bounds.bottom.toFloat()
            val left = bounds.left.toFloat() + paint.strokeWidth
            val right = bounds.right.toFloat() - paint.strokeWidth

            path.reset()
            dividerPath.reset()

            try {
                val isUnder = adapter.isUnder(position)
                if (isUnder) {
                    paint.color = isUnderColor
                } else {
                    paint.color = isOverColor
                }
            } catch (e: Exception) {
                return
            }

            when (viewType) {
                MealsAdapter.VIEW_TYPE_FIRST_IN_DAY -> {
                    top += paint.strokeWidth

                    path.moveTo(left, bottom)
                    path.lineTo(left, top)
                    path.lineTo(right, top)
                    path.lineTo(right, bottom)

                    c.drawPath(path, backgroundRoundedPaint)

                    dividerPath.moveTo(left, bottom)
                    dividerPath.lineTo(right, bottom)
                    c.drawPath(dividerPath, dividerPaint)

                    c.drawPath(path, paint)
                }
                MealsAdapter.VIEW_TYPE_LAST_IN_DAY -> {
                    bottom -= paint.strokeWidth + offset

                    path.moveTo(left, top)
                    path.lineTo(left, bottom)
                    path.lineTo(right, bottom)
                    path.lineTo(right, top)

                    c.drawPath(path, backgroundRoundedPaint)
                    c.drawPath(path, paint)
                }
                MealsAdapter.VIEW_TYPE_EQUAL_IN_DAY -> {
                    path.moveTo(left, top)
                    path.lineTo(left, bottom)
                    path.lineTo(right, bottom)
                    path.lineTo(right, top)
                    path.lineTo(left, top)
                    c.drawPath(path, backgroundPaint)

                    dividerPath.moveTo(left, bottom)
                    dividerPath.lineTo(right, bottom)
                    c.drawPath(dividerPath, dividerPaint)

                    path.reset()
                    path.moveTo(left, top)
                    path.lineTo(left, bottom)
                    path.moveTo(right, top)
                    path.lineTo(right, bottom)
                    c.drawPath(path, paint)
                }
                MealsAdapter.VIEW_TYPE_SINGLE_IN_DAY -> {
                    top += paint.strokeWidth
                    bottom -= paint.strokeWidth + offset

                    path.moveTo(left, top)
                    path.lineTo(left, bottom)
                    path.lineTo(right, bottom)
                    path.lineTo(right, top)
                    path.lineTo(left, top)
                    c.drawPath(path, backgroundRoundedPaint)

                    path.reset()
                    path.moveTo(left, top)
                    path.lineTo(left, bottom)
                    path.lineTo(right, bottom)
                    path.lineTo(right, top)
                    path.lineTo(left, top)
                    c.drawPath(path, paint)
                }
            }
        }
        c.restore()
    }
}