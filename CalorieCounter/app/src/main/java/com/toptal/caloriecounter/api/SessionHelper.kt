package com.toptal.caloriecounter.api

import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject

class SessionHelper @Inject constructor(private val sharedPrefs: SharedPreferences) {
    fun saveSession(cookie: String) = sharedPrefs.edit(commit = true) { putString(sessionKey, cookie) }

    fun loadSession(): String = sharedPrefs.getString(sessionKey, "")!!

    fun clearSession() = sharedPrefs.edit { remove(sessionKey) }

    companion object {
        private const val sessionKey = "session.cookie"
    }
}