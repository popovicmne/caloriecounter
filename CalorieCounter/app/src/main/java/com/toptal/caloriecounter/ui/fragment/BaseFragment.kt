package com.toptal.caloriecounter.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.toptal.caloriecounter.di.Injectable
import javax.inject.Inject

abstract class BaseFragment : Fragment(), Injectable {
    @Inject
    lateinit var injectableViewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        state: Bundle?
    ): View? {
        return inflater.inflate(provideLayoutId(), container, false)
    }

    open fun provideLayoutId() = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(provideToolbar())
        initUI(savedInstanceState)
        initViewModel(savedInstanceState)
    }

    open fun provideToolbar(): Toolbar? = null

    inline fun <reified T> provideViewModel(owner: ViewModelStoreOwner = this): T where T : ViewModel {
        return ViewModelProvider(
            owner,
            injectableViewModelFactory
        ).get(T::class.java)
    }

    open fun initUI(savedInstanceState: Bundle?) {}
    open fun initViewModel(savedInstanceState: Bundle?) {}
}


