package com.toptal.caloriecounter.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.observe
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.Moshi
import com.toptal.caloriecounter.R
import com.toptal.caloriecounter.db.MoshiProvider
import com.toptal.caloriecounter.entity.Meal
import com.toptal.caloriecounter.entity.StatusSuccess
import com.toptal.caloriecounter.hideKeyboard
import com.toptal.caloriecounter.viewmodel.EditMealViewModel
import com.toptal.caloriecounter.withArgs
import kotlinx.android.synthetic.main.fragment_edit_meal.*
import javax.inject.Inject

class EditMealFragment : BaseFragment() {
    @Inject
    lateinit var moshi: Moshi

    private val viewModel by lazy { provideViewModel<EditMealViewModel>() }

    private var editButtonTextRes = R.string.edit_meal_button_create

    override fun provideLayoutId(): Int = R.layout.fragment_edit_meal

    override fun provideToolbar(): Toolbar? {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        return toolbar
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        bindProgressButton(editButton)
        editButton.attachTextChangeAnimator()
        editButton.setOnClickListener {
            var valid = true

            val textString = mealText.text.toString()
            val caloriesString = calories.text.toString()

            if (textString.isEmpty()) {
                mealTextContainer.error = resources.getString(R.string.edit_meal_text_error)
                valid = false
            } else {
                mealTextContainer.error = null
            }

            var calories = -1
            try {
                calories = caloriesString.toInt()
                caloriesContainer.error = null
            } catch (e: Exception) {
                caloriesContainer.error = resources.getString(R.string.edit_meal_calories_error)
                valid = false
            }

            if (valid) {
                editButton.showProgress {
                    buttonTextRes = R.string.edit_meal_progress_text
                    progressColor = R.color.nord0
                }

                hideKeyboard()
                view.requestFocus()
                viewModel.onEditButtonClicked(textString, calories)
            }
        }

        viewModel.editStatus.observe(viewLifecycleOwner) {
            editButton.hideProgress(editButtonTextRes)
            if (it is StatusSuccess) {
                Snackbar.make(
                    view,
                    getString(R.string.edit_result_success_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                Snackbar.make(
                    view,
                    getString(R.string.edit_result_error_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        val meal = arguments?.getString(MEAL_KEY)?.let {
            moshi.adapter(Meal::class.java).fromJson(it)
        }
        if (meal != null) {
            mealText.setText(meal.text)
            calories.setText(meal.kcal.toString())
            editButtonTextRes = R.string.edit_meal_button_update
            editButton.setText(R.string.edit_meal_button_update)

            viewModel.init(meal)
        }
    }

    companion object {
        private const val MEAL_KEY = "edit.keys.meal"

        fun newInstance(meal: Meal? = null): EditMealFragment = EditMealFragment().withArgs {
            putString(MEAL_KEY, MoshiProvider.moshi.adapter(Meal::class.java).toJson(meal))
        }
    }
}