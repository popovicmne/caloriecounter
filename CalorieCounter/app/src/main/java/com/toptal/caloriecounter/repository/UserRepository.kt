package com.toptal.caloriecounter.repository

import com.toptal.caloriecounter.api.UpdateUserRequest
import com.toptal.caloriecounter.api.UserService
import com.toptal.caloriecounter.db.UserDao
import com.toptal.caloriecounter.entity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userService: UserService,
    private val userDao: UserDao
) {
    suspend fun getUsers() = withContext<Resource<List<User>>>(Dispatchers.IO) {
        try {
            val users = userService.getAll()
            ResourceSuccess(users)
        } catch (e: Exception) {
            ResourceError(e)
        }
    }

    suspend fun fetchUser() = withContext<Resource<User>>(Dispatchers.IO) {
        try {
            val meals = userService.get("todo")
            ResourceSuccess(meals)
        } catch (e: Exception) {
            ResourceError(e)
        }
    }

    suspend fun update(user: User, newName: String, newGoal: Int, newPassword: String? = null) =
        withContext(Dispatchers.IO) {
            try {
                userService.update(user._id, UpdateUserRequest(newName, newGoal, newPassword))
                val newUser = user.copy(name = newName, goal = newGoal)
                userDao.update(newUser)
                StatusSuccess
            } catch (e: Exception) {
                StatusError
            }
        }

    suspend fun delete(user: User) = withContext(Dispatchers.IO) {
        try {
            userService.delete(user._id)
            StatusSuccess
        } catch (e: Exception) {
            StatusError
        }
    }
}